package com.tech.connectfinder.Interface;

public interface Registration {
    void login( final String email, final String password );
    void register( final String name, final String email, final String password, final String phone );
    void switch_screen( boolean account );
    void google_sign_in();
}
