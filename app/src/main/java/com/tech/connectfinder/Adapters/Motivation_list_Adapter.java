package com.tech.connectfinder.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tech.connectfinder.Models.Quote;
import com.tech.connectfinder.R;

import java.util.ArrayList;
import java.util.Queue;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class Motivation_list_Adapter extends RecyclerView.Adapter<Motivation_list_Adapter.MotivationViewHolder> {

    private ArrayList<Quote> QuoteList;
    private Context mContext;

    public Motivation_list_Adapter(ArrayList<Quote> quoteList, Context mContext) {
        QuoteList = quoteList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public Motivation_list_Adapter.MotivationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.motivational_card, parent, false);
        MotivationViewHolder viewHolder = new MotivationViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull Motivation_list_Adapter.MotivationViewHolder holder, int position) {
        holder.bindQuotes( QuoteList.get( position ) );
    }

    @Override
    public int getItemCount() {
        return QuoteList.size();
    }

    public  class MotivationViewHolder extends RecyclerView.ViewHolder
    {
        CircleImageView picture;
        TextView quote;
        TextView Author;
        private Context mContext;
        public MotivationViewHolder(@NonNull View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            picture = itemView.findViewById( R.id.author_pic );
            quote   = itemView.findViewById( R.id.quote );
            Author  = itemView.findViewById( R.id.author_name );
        }

        public void bindQuotes(Quote mquote) {
            picture.setImageResource( mquote.getImage() );
            quote.setText( mquote.getQuote() );
            Author.setText( mquote.getAuthor() );
        }
    }
}
