package com.tech.connectfinder.Fragments;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.tech.connectfinder.Interface.Registration;
import com.tech.connectfinder.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class Login extends Fragment {

    public Login() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final Registration registration = (Registration) getActivity();

        final EditText Email      = getActivity().findViewById( R.id.Log_eamail_ET );
        final EditText Password   = getActivity().findViewById( R.id.Log_password_ET );

        ImageButton login_btn       = getActivity().findViewById( R.id.login_btn );
        ImageButton sign_up_btn     = getActivity().findViewById( R.id.sign_up_btn );
        ImageButton google_signin   = getActivity().findViewById( R.id.google_signin );

        sign_up_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registration.switch_screen(false);
            }
        });

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registration.login( Email.getText().toString().trim(), Password.getText().toString().trim() );
            }
        });

        google_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registration.google_sign_in();
            }
        });
    }

}
