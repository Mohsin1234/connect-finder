package com.tech.connectfinder.Fragments;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tech.connectfinder.Adapters.Motivation_list_Adapter;
import com.tech.connectfinder.Models.Quote;
import com.tech.connectfinder.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Motivations extends Fragment {


    public Motivations() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_motivations, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        RecyclerView recyclerView = getActivity().findViewById( R.id.motivation_quotes );

        ArrayList<Quote> quotes = new ArrayList<>();
        quotes.add( new Quote( R.drawable.elon, "“When something is important enough, you do it even if the odds are not in your favor.”",
                "-Elon Musk" ) );
        quotes.add( new Quote( R.drawable.steve, "he hardest thing when you think about focusing. You think focusing is about saying 'Yes.' No. Focusing is about saying 'No.' And when you say 'No,' you piss off people.",
                "-Steve jobs") );
        Motivation_list_Adapter motivationListAdapter = new Motivation_list_Adapter( quotes, getContext() );

        recyclerView.setAdapter( motivationListAdapter );

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( getActivity() );

        recyclerView.setLayoutManager( layoutManager );
        recyclerView.setHasFixedSize( true );

    }
}
