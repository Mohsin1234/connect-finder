package com.tech.connectfinder.Fragments;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.tech.connectfinder.Interface.Communication;
import com.tech.connectfinder.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Dashboard extends Fragment {

    private ImageButton Premium, Motivation;
    Communication communication;

    public Dashboard() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Premium     = getActivity().findViewById( R.id.premium );
        Motivation  = getActivity().findViewById( R.id.motivation_button );
        communication = (Communication) getActivity();

        Premium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                communication.open_payment();
            }
        });

        Motivation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                communication.open_motivation();
            }
        });
    }
}
