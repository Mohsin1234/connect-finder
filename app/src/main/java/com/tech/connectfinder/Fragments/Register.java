package com.tech.connectfinder.Fragments;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.tech.connectfinder.Interface.Registration;
import com.tech.connectfinder.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class Register extends Fragment {


    public Register() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Registration registration = (Registration) getActivity();

        final EditText Email      = getActivity().findViewById(R.id.Reg_email_ET);
        final EditText Name       = getActivity().findViewById(R.id.Reg_name_ET);
        final EditText Password   = getActivity().findViewById(R.id.Reg_password_ET);
        final EditText Phone      = getActivity().findViewById(R.id.Reg_phone_ET);

        ImageButton sign_in_button      = getActivity().findViewById(R.id.sign_in_btn);
        ImageButton registration_btn    = getActivity().findViewById(R.id.register_btn);

        sign_in_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registration.switch_screen( true );
            }
        });

        registration_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registration.register( Name.getText().toString().trim(), Email.getText().toString().trim(),
                        Password.getText().toString().trim(), Phone.getText().toString());
            }
        });
    }
}
