package com.tech.connectfinder.Models;

public class Quote {
    private Integer image;
    private String Quote;
    private String Author;

    public Quote(Integer image, String quote, String author) {
        this.image = image;
        Quote = quote;
        Author = author;
    }

    public Integer getImage() {
        return image;
    }

    public String getQuote() {
        return Quote;
    }

    public String getAuthor() {
        return Author;
    }
}
