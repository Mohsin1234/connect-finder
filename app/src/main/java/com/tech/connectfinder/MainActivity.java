package com.tech.connectfinder;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.stripe.android.PaymentConfiguration;
import com.tech.connectfinder.Fragments.Dashboard;
import com.tech.connectfinder.Fragments.Go_Premium;
import com.tech.connectfinder.Fragments.Login;
import com.tech.connectfinder.Fragments.Motivations;
import com.tech.connectfinder.Interface.Communication;
import com.tech.connectfinder.Interface.Payment;

public class MainActivity extends AppCompatActivity implements Payment, Communication
{
    private GoogleSignInClient mGoogleSignInClient;

    Toolbar toolbar;
    NavigationView navigationView;
    DrawerLayout mDrawerlayout;

    FirebaseAuth mAuth;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById( R.id.toolbar );
        navigationView=findViewById(R.id.CustomNav);
        mDrawerlayout=findViewById(R.id.drawer);

        setSupportActionBar( toolbar );
        ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        actionBar.setShowHideAnimationEnabled(true);
        actionBar.setTitle( "Dashboard" );

        Dashboard fragment = new Dashboard();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.mainContainer, fragment);
        fragmentTransaction.addToBackStack(fragment.toString());
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                menuItem.setChecked(true);
                // close drawer when item is tapped
                mDrawerlayout.closeDrawers();
                Fragment fragment=null;
                Class fragmentClass = null;
                FragmentManager manager=getSupportFragmentManager();
                FragmentTransaction transaction=manager.beginTransaction();

                int id=menuItem.getItemId();
                switch (id)
                {

                    case R.id.terms:
                        toolbar.setTitle(R.string.terms_and_conditions);
                        break;
                    case R.id.wallet:
                        toolbar.setTitle(R.string.wallet);
                        break;
                    case R.id.contactus:
                        toolbar.setTitle(R.string.contact_us);
                        break;

                    case R.id.logout:

                        logout();
                        break;

                }


//                try {
//                    fragment=(Fragment) fragmentClass.newInstance();
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                } catch (InstantiationException e) {
//                    e.printStackTrace();
//                }
//                transaction.replace(R.id.frameContainer,fragment).commit();
                return true;

            }
        });

        mAuth = FirebaseAuth.getInstance();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        PaymentConfiguration.init(this, "pk_test_frvp7jztbyTuxlwnfWe1qyBj00asIFNTzw" );

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            // Name, email address, and profile photo Url
            String name = user.getDisplayName();
            String email = user.getEmail();
            Uri photoUrl = user.getPhotoUrl();

            // Check if user's email is verified
            boolean emailVerified = user.isEmailVerified();

            // The user's ID, unique to the Firebase project. Do NOT use this value to
            // authenticate with your backend server, if you have one. Use
            // FirebaseUser.getIdToken() instead.
            String uid = user.getUid();
        }
        else
        {
            startActivity(new Intent(MainActivity.this, Login_Activity.class));
            finish();
        }
    }

    private void open_payemnt_sceen()
    {
        Go_Premium fragment = new Go_Premium();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.mainContainer, fragment);
        fragmentTransaction.addToBackStack(fragment.toString());
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    private void logout()
    {
        mAuth.signOut();

        // Google sign out
        mGoogleSignInClient.signOut().addOnCompleteListener(this,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        startActivity(new Intent(MainActivity.this, Login_Activity.class));
                        finish();
                    }
                });
        startActivity(new Intent(MainActivity.this, Login_Activity.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Toast.makeText(this, item.getTitle(),Toast.LENGTH_LONG).show();
        switch (item.getItemId()) {
            case android.R.id.home: {
                mDrawerlayout.openDrawer(GravityCompat.START);
                return true;
            }

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void open_payment() {
        open_payemnt_sceen();
    }

    @Override
    public void open_motivation() {
        Motivations fragment = new Motivations();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.mainContainer, fragment);
        fragmentTransaction.addToBackStack(fragment.toString());
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    @Override
    public void go_premium(int cardNumber) {

    }
}
